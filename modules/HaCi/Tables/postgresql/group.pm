package HaCi::Tables::postgresql::group;
use base 'DBIEasy::postgresql';

sub TABLE { #table name
	'squad'
}

sub SETUPTABLE { # create table unless it doesn't exists?
	0
}

sub CREATETABLE { # table create definition
}

sub columnMapping { # map DBIEasy methods to table column names
	{
		ID					=> 'id',
		name				=> 'name',
		description	=> 'description',
		permissions	=> 'permissions',
		createFrom	=> 'create_from',
		createDate	=> 'create_date',
		modifyFrom	=> 'modify_from',
		modifyDate	=> 'modify_date',
	}
}

1;

# vim:ts=2:sw=2:sws=2
