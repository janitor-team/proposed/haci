package HaCi::Tables::postgresql::user;
use base 'DBIEasy::postgresql';

sub TABLE { #Table Name
	'user'
}

sub SETUPTABLE { # Create Table unless it doesn't exists?
	0
}

sub CREATETABLE { # Table Create Definition
}

sub columnMapping { # map DBIEasy methods to table column names
	{
		ID						=> 'id',
		username			=> 'username',
		password			=> 'password',
		groupIDs			=> 'group_ids',
		description		=> 'description',
		createFrom		=> 'create_from',
		createDate		=> 'create_date',
		modifyFrom		=> 'modify_from',
		modifyDate		=> 'modify_date',
	}
}

1;

# vim:ts=2:sw=2:sws=2
