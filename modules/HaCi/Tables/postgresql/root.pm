package HaCi::Tables::postgresql::root;
use base 'DBIEasy::postgresql';

sub TABLE { #Table Name
	'root'
}

sub SETUPTABLE { # Create Table unless it doesn't exists?
	0
}

sub CREATETABLE { # Table Create Definition
}

sub columnMapping { # map DBIEasy methods to table column names
	{
		ID						=> 'id',
		name					=> 'name',
		description		=> 'description',
		ipv6					=> 'ipv6',
		createFrom		=> 'create_from',
		createDate		=> 'create_date',
		modifyFrom		=> 'modify_from',
		modifyDate		=> 'modify_date',
	}
}

1;

# vim:ts=2:sw=2:sws=2
