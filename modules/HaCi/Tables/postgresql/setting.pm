package HaCi::Tables::postgresql::setting;
use base 'DBIEasy::postgresql';

sub TABLE { #Table Name
	'setting'
}

sub SETUPTABLE { # Create Table unless it doesn't exists?
	0
}

sub CREATETABLE { # Table Create Definition
}

sub columnMapping { # map DBIEasy methods to table column names
	{
		ID						=> 'id',
		userID				=> 'user_id',
		param					=> 'param',
		value					=> 'value',
	}
}

1;

# vim:ts=2:sw=2:sws=2
