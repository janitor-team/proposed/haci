package HaCi::Tables::postgresql::networkLock;
use base 'DBIEasy::postgresql';

sub TABLE { #Table Name
	'network_lock'
}

sub SETUPTABLE { # Create Table unless it doesn't exists?
	0
}

sub CREATETABLE { # Table Create Definition
}

sub columnMapping { # map DBIEasy methods to table column names
	{
		ID		=> 'id',
		ts		=> 'ts',
		duration	=> 'duration',
		rootID		=> 'root_id',
		networkPrefix	=> 'network_prefix',
		hostPart	=> 'host_part',
		cidr		=> 'cidr',
		ipv6		=> 'ipv6',
	}
}

1;

# vim:ts=2:sw=2:sws=2
